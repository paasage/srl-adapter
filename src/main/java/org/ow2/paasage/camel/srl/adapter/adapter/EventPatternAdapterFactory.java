/*
 * Copyright (C) 2015 University of Ulm.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/
 */

package org.ow2.paasage.camel.srl.adapter.adapter;

import eu.paasage.camel.scalability.EventPattern;
import org.ow2.paasage.camel.srl.adapter.communication.FrontendCommunicator;

/**
 * Created by Frank on 06.09.2015.
 */
public interface EventPatternAdapterFactory {
    Adapter create(FrontendCommunicator fc, EventPattern eventPattern);
}
